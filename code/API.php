<?php

/**
 * Class IntelliJ IDEA.
 * User: macairpremper
 * Date: 4/21/15
 * Time: 2:57 PM
 */
class API extends ModelAdmin
{
	static $managed_models = array('ApiUser');
	static $url_segment = 'api';

	public function getEditForm($id = null, $fields = null)
	{
		$form = parent::getEditForm($id, $fields);

		/** @var GridField $listField */
		$listField = $form->Fields()->fieldByName($this->modelClass);
		if ($gridField = $listField->getConfig()->getComponentByType('GridFieldDetailForm'))
			$gridField->setItemRequestClass('ApiProjectFieldDetailForm_ItemRequest');

		return $form;
	}
}

class ApiProjectFieldDetailForm_ItemRequest extends GridFieldDetailForm_ItemRequest
{

	static $allowed_actions = array(
		'ItemEditForm'
	);

	public function ItemEditForm()
	{
		$form = parent::ItemEditForm();
		$formActions = $form->Actions();

		if ($actions = $this->record->getCMSActions())
			foreach ($actions as $action)
				$formActions->push($action);

		return $form;
	}

	public function doSaveCompile($data, Form $form)
	{
		/** @var ApiProject $project */
		$project = ApiProject::get_by_id('ApiProject', 2);
		//$client->write();
		print_r($project->compile());
	}

}    // end class ApiClientFieldDetailForm_ItemRequest
