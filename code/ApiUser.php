<?php

/**
 * Class IntelliJ IDEA.
 * User: macairpremper
 * Date: 4/21/15
 * Time: 2:49 PM
 */
class ApiUser extends Member
{

	static $db = array(
		'Username' => 'Varchar'
	);

	static $has_many = array(
		'Projects' => 'ApiProject'
	);

	static $indexes = array(
		'Prefix' => true
	);

	static $summary_fields = array(
		'FirstName', 'Surname', 'Username'
	);
}
