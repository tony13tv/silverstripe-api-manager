<?php

/**
 * Class IntelliJ IDEA.
 * User: macairpremper
 * Date: 4/21/15
 * Time: 2:50 PM
 */
class ApiDBField extends DataObject
{
	static $db = array(
		'Name' => 'Varchar',
		'Type' => 'Varchar'
	);

	static $has_one = array(
		'Class' => 'ApiClass'
	);

	static $summary_fields = array(
		'Name', 'Type'
	);
}
