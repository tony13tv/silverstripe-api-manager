<?php

/**
 * Class IntelliJ IDEA.
 * User: macairpremper
 * Date: 4/21/15
 * Time: 6:55 PM
 */
class ApiRelation extends DataObject
{
	static $db = array(
		'Name' => 'Varchar'
	);

	static $has_one = array(
		'RelatedApiClass' => 'ApiClass',
		'ApiClass' => 'ApiClass'
	);

	public function getCMSFields()
	{
		$fields = parent::getCMSFields();
		//if ($this->ID) {
		/** @var DropdownField $relatedApiClass */
		$relatedApiClass = $fields->dataFieldByName('RelatedApiClassID');
		$relatedApiClass->setValue($this->RelatedApiClassID);
		$relatedApiClass->setSource(ApiClass::get()->filter(array('ProjectID' => $this->ApiClass()->ProjectID))->map()->toArray());
		$fields->replaceField('RelatedApiClassID', $relatedApiClass);
		//}
		return $fields;
	}
}
