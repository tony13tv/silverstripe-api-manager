<?php

/**
 * Class IntelliJ IDEA.
 * User: macairpremper
 * Date: 4/22/15
 * Time: 9:27 AM
 */
class ApiProject extends DataObject
{
	static $db = array(
		'Name' => 'Varchar'
	);
	static $has_one = array(
		'Owner' => 'ApiUser'
	);
	static $has_many = array(
		'Classes' => 'ApiClass'
	);

	public function getCMSActions()
	{
		$actions = parent::getCMSActions();
		$actions->push($doSaveCompile = new FormAction('doSaveCompile', 'Save & Compile'));
		$doSaveCompile->setUseButtonTag(true)
			->addExtraClass('ss-ui-action-constructive')
			->setAttribute('data-icon', 'accept');
		return $actions;
	}

	public function compile()
	{
		$fs = new Filesystem();
		$dirname = CLIENTS_PATH . '/code/' . $this->FirstName;
		if (!file_exists($dirname)) {
			$fs->makeFolder($dirname);
		}
		$status = "";
		if (is_dir($dirname)) {
			$managed_models = array();
			foreach ($this->Classes() as $class) {
				$filename = $dirname . '/' . $this->Username . '_' . $class->Name;
				$dbfields = $hasones = $hasmanys = $manymanys = array();
				foreach ($class->DBFields() as $dbfield) {
					$dbfields[] = "'$dbfield->Name' => '$dbfield->Type'";
				}

				foreach ($class->HasOne() as $hasone) {
					$hasones[] = "'$hasone->Name' => '{$this->Username}_{$hasone->RelatedApiClass()->Name}'";
				}
				foreach ($class->HasMany() as $hasmany) {
					$hasmanys[] = "'$hasmany->Name' => '{$this->Username}_{$hasmany->RelatedApiClass()->Name}'";
				}
				foreach ($class->ManyMany() as $manymany) {
					$manymanys[] = "'$manymany->Name' => '{$this->Username}_{$manymany->RelatedApiClass()->Name}'";
				}
				$dbfields = join(',', $dbfields);
				$hasones = join(',', $hasones);
				$hasmanys = join(',', $hasmanys);
				$manymanys = join(',', $manymanys);
				$content = <<<PHP
<?php
class {$this->Username}_{$class->Name} extends DataObject {
	static \$api_access = true;

	static \$db = array(
		$dbfields
	);

	static \$has_one = array(
		$hasones
	);

	static \$has_many = array(
		$hasmanys
	);

	static \$many_many = array(
		$manymanys
	);
}
PHP;
				if ($this->writeToFile($filename . '.php', $content)) {
					$status .= "Class $class->Name created!";
				} else {
					$status .= "Class $class->Name not created!";
				}
				$managed_models[] = "'{$this->Prefix}_{$class->Name}'";
			}
			$managed_models = join(',', $managed_models);
			$filename = $dirname . '/' . $this->Username . 'Admin.php';
			$content = <<<PHP
<?php
class {$this->Username}_Admin extends ModelAdmin {
	static \$url_segment = '$this->Name';

	static \$managed_models = array(
		$managed_models
	);
}
PHP;
			if ($this->writeToFile($filename, $content)) {
				$status .= "ModelAdmin {$this->Username}Admin for client $this->Name created!";
			} else {
				$status .= "ModelAdmin {$this->Username}Admin for client $this->Name not created!";
			}

		}
		/** @var DatabaseAdmin $da */
		$_GET['flush'] = 'all';
		$da = DatabaseAdmin::create();
		$da->build();
		return "$status Api created Successfully";
	}

	private function writeToFile($filename, $content)
	{
		return (@$fh = fopen($filename, 'wb')) && fwrite($fh, $content) && fclose($fh);
	}
}
