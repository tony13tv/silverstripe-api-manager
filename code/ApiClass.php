<?php

/**
 * Class IntelliJ IDEA.
 * User: macairpremper
 * Date: 4/21/15
 * Time: 2:49 PM
 */
class ApiClass extends DataObject
{
	static $db = array(
		'Name' => 'Varchar'
	);

	static $has_many = array(
		'DBFields' => 'ApiDBField',
		'HasOne' => 'ApiHasOneRelation',
		'HasMany' => 'ApiHasManyRelation',
		'ManyMany' => 'ApiManyManyRelation',
	);

	static $has_one = array(
		'Project' => 'ApiProject'
	);
}
